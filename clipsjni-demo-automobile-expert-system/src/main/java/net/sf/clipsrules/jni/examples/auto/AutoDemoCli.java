package net.sf.clipsrules.jni.examples.auto;

import net.sf.clipsrules.jni.*;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.*;

public class AutoDemoCli {

    private enum InterviewState
    {
        GREETING,
        INTERVIEW,
        CONCLUSION
    };

    ResourceBundle autoResources;

    Environment clips;

    String relationAsserted;
    ArrayList<String> variableAsserts;
    ArrayList<String> priorAnswers;
    InterviewState interviewState;
    String userInput;
    CaptureRouter captureClipsOutput;

    Scanner in;

    private void processRules() throws CLIPSException {
        clips.reset();

        for (String factString : variableAsserts) {
            String assertCommand = "(assert " + factString + ")";
            clips.eval(assertCommand);
        }
    }

    private void setUpCurrentState() throws Exception {
        /*===========================*/
        /* Get the current UI state. */
        /*===========================*/

        FactAddressValue fv = clips.findFact("UI-state");

        /*========================================*/
        /* Determine the Next/Prev button states. */
        /*========================================*/

        if (fv.getSlotValue("state").toString().equals("conclusion")) {
            interviewState = InterviewState.CONCLUSION;
        }
        else if (fv.getSlotValue("state").toString().equals("greeting")) {
            interviewState = InterviewState.GREETING;
        }
        else {
            interviewState = InterviewState.INTERVIEW;
        }

        /*=====================*/
        /* Set up the choices. */
        /*=====================*/

        MultifieldValue damf = (MultifieldValue) fv.getSlotValue("display-answers");
        MultifieldValue vamf = (MultifieldValue) fv.getSlotValue("valid-answers");

        String validAnswers = "";
        for (int i = 0; i < damf.size(); i++) {
            LexemeValue da = (LexemeValue) damf.get(i);
            LexemeValue va = (LexemeValue) vamf.get(i);

            validAnswers += (va.getValue() + ";");
        }

        /*====================================*/
        /* Set the label to the display text. */
        /*====================================*/

        relationAsserted = ((LexemeValue) fv.getSlotValue("relation-asserted")).getValue();

        /*====================================*/
        /* Set the label to the display text. */
        /*====================================*/

        String theText = ((StringValue) fv.getSlotValue("display")).getValue();

        displayPrompt(theText, validAnswers);
    }

    private boolean handleInput() {
        String answer =  userInput;
        String relationToAssetStr = "(" + relationAsserted + " " + answer + ")";

        switch (interviewState) {
            case GREETING:
                if("no".equals(answer)) {
                    return false;
                }
            case INTERVIEW:
                variableAsserts.add(relationToAssetStr);
                priorAnswers.add(answer);
                break;

            case CONCLUSION:
                variableAsserts.clear();
                priorAnswers.clear();
                if("no".equals(userInput)) {
                    return false;
                }
                break;
        }

        return true;
    }

    private void displayPrompt(String theText, String theAnswers) {
        System.out.println(theText);

        if(interviewState == InterviewState.GREETING) {
            System.out.println("Do you want to start?");
            theAnswers = "no;" + theAnswers;
        } else if(interviewState == InterviewState.CONCLUSION) {
            System.out.println("Do you want to start again?");
            theAnswers = "no;yes";
        }

        System.out.println("Answer with " + theAnswers);
    }

    private void readInput() {
        userInput = in.next();
    }

    public static void main(String[] args) {
        new AutoDemoCli().runAutoCli();
    }

    AutoDemoCli() {
        try {
            autoResources = ResourceBundle.getBundle("AutoResources", Locale.getDefault());
        } catch (MissingResourceException mre) {
            mre.printStackTrace();
            return;
        }

        in = new Scanner(System.in);


        variableAsserts = new ArrayList<String>();
        priorAnswers = new ArrayList<String>();

        clips = new Environment();

        captureClipsOutput = new CaptureRouter(clips, new String [] { Router.STDOUT });

        try {
            clips.loadFromResource("/auto.clp");
            try {
                clips.loadFromResource("/auto_" +
                        Locale.getDefault().getLanguage() + ".clp");
            } catch (FileNotFoundException fnfe) {
                if (Locale.getDefault().getLanguage().equals("en")) {
                    throw fnfe;
                } else {
                    clips.loadFromResource("/auto_en.clp");
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
            return;
        }
    }

    void runAutoCli() {
        try {
            do {
                processRules();
                clips.run();
                setUpCurrentState();
                readInput();
                System.out.println(captureClipsOutput.getOutput());
            } while (handleInput());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
