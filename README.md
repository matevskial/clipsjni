# CLIPS JNI

This repository is a fork of the original clips code found at https://sourceforge.net/p/clipsrules/code/HEAD/tree/

The purpose is to create a jar package that can be published on a repository(such as maven central)

# Building libCLIPSJNI.so

You need to have gcc and java JDK installed on your computer.

Open either makefile.lnx, makefile.mac, or makefile.win(depending on your operating system) 
and edit the variable JAVA_HOME to point to your java JDK installation.

Then use the make command to initiate the building.

Example in linux:

```bash
make -f makefile.lnx
```

After the build is complete, you should see either ```libCLIPSJNI.so```, ```libCLIPSJNI.dll``` or ```libCLIPSJNI.jnilib```,]
depending on your operating system.

# Running the automobile-expert-system example

## Running with IDE

Make sure you specify the working directory to the root of this repository 
and add VM option ```-Djava.library.path=clipsjni-java/lib``` in the run configuration for  ```AutoDemo``` and ```AutoDemoCli```

Then you should be able to run either ```AutoDemo``` or ```AutoDemoCli``` normally.

## Running from command line

Go to the root of this project and use maven to compile the project and generate jar files

```bash
./mvnw clean install
```

Run ```AutoDemo```

```bash
java -Djava.library.path=clipsjni-java/lib -jar <name-of-jar>
```

where ```<name-of-jar>``` is the generated jar file in ```clipsjni-demo-automobile-expert-system/target``` directory.

Note: On Linux, for a better looking java swing gui, execute

```bash
export _JAVA_OPTIONS="-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dsun.java2d.xrender=true"
```

before executing the jar.

# Running the CLIPS IDE

Make sure you specify the working directory to the root of this repository
and add VM option ```-Djava.library.path=clipsjni-java/lib``` in the run configuration for  ```AutoDemo``` and ```AutoDemoCli```

Then you should be able to run ```CLIPSIDE``` normally.

## Running from command line

Go to the root of this project and use maven to compile the project and generate jar files

```bash
./mvnw clean install
```

Run ```AutoDemo```

```bash
java -Djava.library.path=clipsjni-java/lib -jar <name-of-jar>
```

where ```<name-of-jar>``` is the generated jar file in ```clipsjni-ide/target``` directory.

Note: On Linux, for a better looking java swing gui, execute

```bash
export _JAVA_OPTIONS="-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dsun.java2d.xrender=true"
```
